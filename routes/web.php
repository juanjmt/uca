<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('notas');
});*/
/*

Route::get('/',['as'=>'notas.index','uses'=>'NotasController@index']);*/
/*
use GuzzleHttp\Client;
Route::get('/', function () {


    //return view('notas');
});

*/

//Route::get('/','NotasController@index');
/*
Route::get('/', function () {
    return view('facultades');
});*/
Route::get('/',['as'=>'facultades.index','uses'=>'FacultadesController@index']);
Route::post('/',['as'=>'facultades.show','uses'=>'FacultadesController@show']);

Route::resource('estudiantes', 'EstudianteController');
Route::resource('usuarios', 'UsuarioController');


/*
Route::get('estudiantes/create',['as'=>'estudiantes.create','uses'=>'EstudianteController@create']);
Route::post('estudiantes/store',['as'=>'estudiantes.store','uses'=>'EstudianteController@store']);
*/
/*
Route::get('estudiantes.create', 'EstudianteController@create');
Route::post('estudiantes.store', 'EstudianteController@store');*/
