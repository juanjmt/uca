<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="/css/app.css">

    </head>
    <body>
        <div class="container">
            <h1>Procesar Notas</h1>
            <br>
            <form  action="{{ URL::asset('/') }}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <select id="facu" name="facu"  class="form-control" >
                             <option selected="selected" disabled>Facultad</option>
                            @foreach($datos['facu'] as $facu)
                              <option value="{{$facu->idfacu}}">{{$facu->nomfacu}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <select  id="carre" name="carre"  class="form-control" >
                             <option selected="selected" disabled>Carrera</option>
                             @foreach($datos['carrera'] as $car)
                              <option value="{{$car->id}}">{{$car->nomcarrera}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <select id="mate" name="mate"   class="form-control" >
                             <option  selected="selected" disabled>Materia</option>
                            @foreach($datos['materia'] as $mat)
                              <option value="{{$mat->id}}">{{$mat->nommateria}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <select id="seme" name="seme"   class="form-control" >
                             <option   selected="selected" disabled>Semestre</option>
                            @foreach($datos['semestre'] as $sem)
                              <option value="{{$sem->id}}">{{$sem->numsemestre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <select id="tur" name="tur"  class="form-control" >
                             <option  selected="selected" disabled>Turno</option>
                            @foreach($datos['turnos'] as $turno)
                              <option value="{{$turno->id}}">{{$turno->nomturno}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-lg-2 col-md-2 col-sm-4 col-xs-6">
                        <button class="btn btn-primary">Search</button>
                    </div>
            </div>
            </form>
            <br>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                <div class="col-12">
                    <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">DNI</th>
                      <th scope="col">NOMBRE</th>
                      <th scope="col">APELLIDO</th>
                      <th scope="col">Calificación</th>
                    </tr>
                  </thead>
                  <?php $pos=0 ?>
                    @foreach($datos['alumnos'] as $alum)
                         <?php $pos++ ?>
                        <tbody>
                        <tr>
                            <td>{{$pos}}</td>
                            <td>{{$alum->dni}}</td>
                            <td>{{$alum->nombre}}</td>
                            <td>{{$alum->apellido}}</td>
                            <td><input class="form-control" type="text" id="" name="" style="width:40px;"></td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                <div class="col col-lg-12  col-md-12  col-sm-12  col-xs-12 text-center">
                    <button class="btn btn-primary">Guardar</button>
                </div>
                </div>

            </div>
                </div>
            </div>
    </div>

    </body>
</html>
