@extends('app')
@section('content')
<h1> Registrar alumnos </h1>
    <form  method="POST" action="{{ route('estudiantes.store') }}">
            @include('estudiantes.partials.form')
            <br>
        <button type="submit" name="button" class="btn btn-primary">Guardar</button>
    </form>
@endsection
