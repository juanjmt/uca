<div class="row">
    @csrf <!-- {{ csrf_field() }} -->
    <div class="col-md-12">
        <label for="dni">Dni:</label><br>
        <input type="text" class="form-control" name="dni" id="dni" value="">
        @if($errors->has('dni'))
            <p class="alert alert-danger">{{$errors->first('dni')}}</p>
        @endif
    </div>
    <div class="col-md-12">
        <label for="dni">Nombres:</label><br>
        <input type="text" class="form-control" name="nombre" id="nombre" value="">
        @if($errors->has('nombre'))
            <p class="alert alert-danger">{{$errors->first('nombre')}}</p>
        @endif
    </div>
    <div class="col-md-12">
        <label for="dni">Apellidos:</label><br>
        <input type="text" class="form-control" name="apellido" id="apellido" value="">
        @if($errors->has('apellido'))
            <p class="alert alert-danger">{{$errors->first('apellido')}}</p>
        @endif
    </div>
</div>
