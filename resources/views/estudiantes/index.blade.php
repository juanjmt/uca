@extends('app')
@section('content')
<h1> Listado de alumnos</h1>
    <div class="row">
        <div class="col-md-12 text-right" style="margin:10px;" >
            <a href="{{ url('/estudiantes/create') }}" class="btn btn-primary">New</a>
        </div>
    </div>
    <div id="contendorflex">

        <table class="table table-bordered table-striped">
            <thead>
                <tr><th>DNI</th><th>Nombres</th><th>Apellidos</th></tr>
            </thead>
            <tbody>
                @foreach($estudiantes as $estudiante)
                    <tr>
                        <td>{{$estudiante->dni}}</td>
                        <td>{{$estudiante->nombre}}</td>
                        <td>{{$estudiante->apellido}}</td>
                    </tr>
                @endforeach

            </tbody>

        </table>
        {!! $estudiantes->render(); !!}
    </div>

@endsection
