@extends('app')
@section('content')
    <h1> Registrar Usuarios </h1>
    <br>
    <form class="" action="{{ route('usuarios.store') }}" method="POST">
        @csrf <!-- {{ csrf_field() }} -->
        @include('usuarios.form.formuser');
    </form>
@endsection
