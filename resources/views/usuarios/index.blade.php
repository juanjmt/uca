@extends('app')
<?php
    $message=Session::get('message');
 ?>
 @if($message=='store')
 <div class="alert alert-success" role="alert">
     Los datos se han guardado con exito
</div>
 @endif
@section('content')
<h1> Listado de Usuarios</h1>
    <div class="row">
        <div class="col-md-12 text-right" style="margin:10px;" >
            <a href="{{ url('/usuarios/create') }}" class="btn btn-primary">New User</a>
        </div>
    </div>
    <div id="contendorflex">

        <table class="table table-bordered table-striped">
            <thead>
                <tr><th>#</th><th>Username</th><th>Email</th><th>Operaciones</th></tr>
            </thead>
            <tbody>
                <?php  $pos=0; ?>
                @foreach($usuarios as $user)
                <?php  $pos++; ?>
                    <tr>
                        <td>{{$pos}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>

                        <td><a href="{{ url('/usuarios/'.$user->id.'/edit') }}"><i class="fas fa-edit"></i></a></td>
                    </tr>
                @endforeach

            </tbody>

        </table>
        {!! $usuarios->render(); !!}
    </div>

@endsection
