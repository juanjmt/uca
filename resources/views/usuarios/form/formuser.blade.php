<div class="row">
    <div class="col-md-12">
        <label for="">UserName:</label><br>
        <input type="text" class="form-control" name="username" id="username" value="@if(isset($user)) {{$user->username }} @endif">
    </div>
    <div class="col-md-12">
        <label for="">Email:</label><br>
        <input type="text" class="form-control" name="email" id="email" value="@if(isset($user)) {{$user->email }} @endif">
    </div>
    <div class="col-md-12">
        <label for="">Password:</label><br>
        <input type="password" class="form-control" name="password" id="password" value="">
    </div>

    <div class="col-md-12">
        <br><br>
        <button type="submit"  class="btn btn-primary" name="button">Guardar</button>
    </div>
</div>
