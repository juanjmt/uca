@extends('app')
@section('content')
    <h1> Editar Usuarios </h1>
    <br>
    <form class="" action="{{ route('usuarios.update',$user->id) }}" method="POST">
        {{method_field('PATCH')}}
        {{ csrf_field() }}
        @include('usuarios.form.formuser');
    </form>
@endsection
