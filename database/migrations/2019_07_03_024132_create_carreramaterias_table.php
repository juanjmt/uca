<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarreramateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreramaterias', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('carrera_id')->unsigned();
            $table->integer('materia_id')->unsigned();
            $table->foreign('carrera_id')->references('id')->on('carreras');
            $table->foreign('materia_id')->references('id')->on('materias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreramaterias');
    }
}
