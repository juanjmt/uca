<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inscripciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('inscripciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alumno_id')->unsigned();
            $table->integer('carreramateria_id')->unsigned();
            $table->integer('turno_id')->unsigned();
            $table->foreign('alumno_id')->references('id')->on('estudiantes');
            $table->foreign('carreramateria_id')->references('id')->on('carreramaterias');
            $table->foreign('turno_id')->references('id')->on('turnos');
            $table->string('year',4);
            $table->decimal('nota',5,2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
