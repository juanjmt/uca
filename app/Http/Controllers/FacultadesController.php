<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facultad; // Instanciamos el modelo
use App\Turno;
use App\Carrera;
use App\Materia;
use App\Semestre;
use App\Estudiante;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;


class FacultadesController extends Controller
{
    public function index()
   {
       $facu = Facultad::all();
       $carrera = Carrera::all();
       $materia = Materia::all();
       $turnos = Turno::all();
       $semestre = Semestre::all();
       $alum= Estudiante::all();

       $datos = ['facu' => $facu,'carrera' => $carrera,'materia' => $materia,'turnos' => $turnos,'semestre' => $semestre,'alumnos'=>$alum];
       //$facu='framacoia';
       return view('facultades', compact('datos'));
   }

   public function show(Request $request)
   {
       if ($request['facu']=='' || $request['facu']==null){
           $facu = Facultad::all();
           $carrera = Carrera::all();
           $materia = Materia::all();
           $turnos = Turno::all();
           $semestre = Semestre::all();
           $alum = Estudiante::all();
       }else {
           $alum=DB::table('estudiantes')
           ->join('inscripciones','inscripciones.alumno_id','=','estudiantes.id')
           ->join('carreramaterias','carreramaterias.id','=','inscripciones.carreramateria_id')
           ->join('materias','materias.id','=','carreramaterias.materia_id')
           ->join('carreras','carreras.id','=','carreramaterias.carrera_id')
           ->join('facultades','facultades.id','=','carreras.facultad_id')
           ->join('semestres','semestres.id','=','materias.semestre_id')
           ->join('turnos','turnos.id','=','inscripciones.turno_id')
           ->select('estudiantes.dni','estudiantes.nombre','estudiantes.apellido')
           ->where('facultades.id', '=',$request['facu'])
           ->get();

       }


       $datos = ['facu' => $facu,'carrera' => $carrera,'materia' => $materia,'turnos' => $turnos,'semestre' => $semestre,'alumnos'=>$alum];
       return view('facultades', compact('datos'));
   }

}
