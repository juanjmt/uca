<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante;
use Illuminate\Support\Facades\DB;


class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $resul=DB::table('estudiantes')
                   ->join('inscripciones','inscripciones.alumno_id','=','estudiantes.id')
                   ->join('carreramaterias','carreramaterias.id','=','inscripciones.carreramateria_id')
                   ->join('materias','materias.id','=','carreramaterias.materia_id')
                   ->join('carreras','carreras.id','=','carreramaterias.carrera_id')
                   ->join('facultades','facultades.id','=','carreras.facultad_id')
                   ->join('semestres','semestres.id','=','materias.semestre_id')
                   ->join('turnos','turnos.id','=','inscripciones.turno_id')
                   ->select('estudiantes.dni','estudiantes.nombre','estudiantes.apellido','facultades.nomfacu','carreras.nomcarrera','materias.nommateria')
                   ->get();
        return $resul;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

         return     DB::table('estudiantes')
                    ->join('inscripciones','inscripciones.alumno_id','=','estudiantes.id')
                    ->join('carreramaterias','carreramaterias.id','=','inscripciones.carreramateria_id')
                    ->join('materias','materias.id','=','carreramaterias.materia_id')
                    ->join('carreras','carreras.id','=','carreramaterias.carrera_id')
                    ->join('facultades','facultades.id','=','carreras.facultad_id')
                    ->join('semestres','semestres.id','=','materias.semestre_id')
                    ->join('turnos','turnos.id','=','inscripciones.turno_id')
                    ->select('estudiantes.dni','estudiantes.nombre','estudiantes.apellido',)
                    ->where('facultades.id', '=',$request['facu'])
                    ->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
