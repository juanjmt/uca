<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    //
    protected $primary_key = 'id';
    protected $fillable=['nomcarrera','facultad_id'];
    protected $table='carreras';
    public function facultad(){
        return $this->belongsTo('App\Facultad','id');
    }
    public function carreramateria(){
        return $this->hasMany('App\Carreramateria','carrera_id');
    }
}


?>
