<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    //
    //
    protected $primary_key = 'id';
    protected $fillable=['nommateria','semestre_id'];
    protected $table='materias';
    public function semestre(){
        return $this->belongsTo('App\Semestre','id');
    }
    public function carreramateria(){
        return $this->hasMany('App\Carreramateria','materia_id');
    }
}
