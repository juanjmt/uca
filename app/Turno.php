<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    //
    protected $primary_key = 'id';
    protected $fillable=['nomturno'];
    protected $table='turnos';
    public function inscripciones(){
        return $this->hasMany('App\Inscripcione','turno_id');
    }
}
