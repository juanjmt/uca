<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    //
    protected $primary_key = 'id';
    protected $fillable=['nomfacu','facultad_id'];
    protected $table='facultades';
    public function carreras(){
        return $this->hasMany('App\Carrera','facultad_id');
    }
}


?>
