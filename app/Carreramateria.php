<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carreramateria extends Model
{
    //
    protected $primary_key = 'id';
    protected $fillable=['carrera_id','materia_id'];
    protected $table='carreramateria';
    public function carrera(){
        return $this->belongsTo('App\Carrera','id');
    }
    public function materia(){
        return $this->belongsTo('App\Materia','materia');
    }
}
