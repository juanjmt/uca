<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    //
    protected $table='estudiantes';
    protected $primary_key = 'id';
    protected $fillable=['dni','nombre','apellido'];
    
    public function inscripcion()
    {
        return $this->hasMany('App\Inscripcion','alumno_id');
        // code...
    }
}
