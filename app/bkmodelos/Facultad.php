<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    //
    protected $table='facultades';
    protected $primary_key = 'idfacu';
    protected $fillable=['nomfacu'];

    public function carrera()
    {
        return $this->hasMany('App\Carrera','idfacu');
        // code...
    }
}
