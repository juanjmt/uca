<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    //
    protected $table='carreras';
    protected $primary_key = 'idcarreras';
    protected $fillable=['nomcarrera','idfacu'];
    public function facultad()
    {
        return $this->belongsTo('App\Facultad', 'idfacu');
        // code...
    }
    public function carreramateria()
    {
        return $this->hasMany('App\Carreramateria', 'idcarreras');

    }
}
