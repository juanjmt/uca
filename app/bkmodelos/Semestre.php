<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semestre extends Model
{
    //
    protected $table='semestres';
    protected $primary_key = 'idsemestres';
    protected $fillable=['numsemestre'];
    public function materia()
    {
        return $this->hasMany('App\Materia','idsemestres');
        // code...
    }
}
