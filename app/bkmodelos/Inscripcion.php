<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    //
    protected $table='inscripcion';
    protected $primary_key = 'idinscripcion';
    protected $fillable=['idalumnos','id_carrera_materia','idturnos','year','nota'];
    public function carremateria()
    {
        return $this->belongsTo('App\Carreramateria','id_carrera_materia');
        // code...
    }
    public function turno()
    {
        return $this->belongsTo('App\Turno','idturnos');
        // code...
    }
    public function estudiante()
    {
        return $this->belongsTo('App\Estudiante','idalumnos');
        // code...
    }

}
