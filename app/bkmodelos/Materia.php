<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    //
    protected $table='materias';
    protected $primary_key = 'idmaterias';
    protected $fillable=['nommateria','idsemestres'];

    public function semestre()
    {
        return $this->belongsTo('App\Semestre','idsemestres');
        // code...
    }
    public function carremateria()
    {
        return $this->hasMany('App\Carreramateria','idmaterias');
        // code...
    }
}
