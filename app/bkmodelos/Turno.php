<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    //
    protected $table='turnos';
    protected $primary_key = 'idturnos';
    protected $fillable=['nomturno'];
    public function inscripcion()
    {
        return $this->hasMany('App\Inscripcion','idturnos');
        // code...
    }
}
