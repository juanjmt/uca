<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    //
    protected $table='alumnos';
    protected $primary_key = 'idalumnos';
    protected $fillable=['dni','nombre','apellido'];
    public function inscripcion()
    {
        return $this->hasMany('App\Inscripcion','idalumnos');
        // code...
    }
}
