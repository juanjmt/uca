<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carreramateria extends Model
{
    //
    protected $table='carrera_materia';
    protected $primary_key = 'id_carrera_materia';
    protected $fillable=['id_carrera_materia','idcarreras','idmaterias'];
    public function carrera()
    {
        return $this->belongsTo('App\Carrera','idcarreras');
        // code...
    }
    public function materia()
    {
        return $this->belongsTo('App\Materia','idmaterias');
        // code...
    }
    public function inscripcion()
    {
        return $this->hasMany('App\Inscripcion','id_carrera_materia');
        // code...
    }

}
