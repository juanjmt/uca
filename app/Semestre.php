<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semestre extends Model
{
    //
    protected $primary_key = 'id';
    protected $fillable=['numsemestre'];
    protected $table='semestres';
    public function materias(){
        return $this->hasMany('App\Materia','semestre_id');
    }
}
